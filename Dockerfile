FROM php:7.2-fpm-alpine

RUN apk update && \
    pecl channel-update pecl.php.net

RUN apk add curl libmemcached-dev libjpeg jpeg-dev \
    libpng-dev freetype-dev openssl-dev libmcrypt-dev && \
    apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS

RUN docker-php-ext-install pdo_mysql \
    && docker-php-ext-configure gd \
    --enable-gd-native-ttf \
    --with-jpeg-dir=/usr/lib \
    --with-freetype-dir=/usr/include/freetype2 && \
    docker-php-ext-install gd

RUN docker-php-ext-install -j$(nproc) opcache && \
    docker-php-ext-enable opcache

RUN apk add libzip-dev zip && \
    docker-php-ext-configure zip --with-libzip && \
    docker-php-ext-install -j$(nproc) zip

RUN apk del .phpize-deps

# $PHP_INI_DIR usually equals /usr/local/etc/php
COPY config/opcache.ini $PHP_INI_DIR/conf.d/
COPY config/api.ini $PHP_INI_DIR/conf.d/
COPY config/api.pool.conf /usr/local/etc/php-fpm.d/

WORKDIR /var/www/api
EXPOSE 9000
